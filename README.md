## rt-oficinas

#### Interfaces

| Interfaz | Direccion       | Mascara         |
| -------- | --------------- | --------------- |
| GE 0/0   | 1.0.0.1         | 255.255.255.252 |
| GE 1/0   | 192.168.100.1   | 255.255.255.128 |
| GE 2/0   | 192.168.100.193 | 255.255.255.192 |



#### Tabla de enrutamiento

| Id de red | Mascara         | via      |
| --------- | --------------- | -------- |
| 10.0.0.4  | 255.255.255.252 | 10.0.02  |
| 200.0.0.0 | 255.255.255.0   | 10.0.0.2 |



## rt-servers

#### Interfaces

| Interfaz | Direccion | Mascara         |
| -------- | --------- | --------------- |
| GE 0/0   | 10.0.0.2  | 255.255.255.252 |
| GE 1/0   | 10.0.0.5  | 255.255.255.252 |



#### Tabla de enrutamiento

| Id de red       | Mascara         | via      |
| --------------- | --------------- | -------- |
| 192.168.100.0   | 255.255.255.128 | 10.0.0.1 |
| 192.168.100.192 | 255.255.255.192 | 10.0.0.1 |
| 200.0.0.0       | 255.255.255.0   | 10.0.0.6 |



## Hosts

#### server-1

| Interfaz | Direccion  | Mascara       | Gateway   |
| -------- | ---------- | ------------- | --------- |
| FE 0     | 200.0.0.10 | 255.255.255.0 | 200.0.0.1 |



#### ventas-1

| Interfaz | Direccion      | Mascara         | Gateway       |
| -------- | -------------- | --------------- | ------------- |
| FE 0     | 192.168.100.10 | 255.255.255.128 | 192.168.100.1 |

#### gerencia-1

| Interfaz | Direccion       | Mascara         | Gateway         |
| -------- | --------------- | --------------- | --------------- |
| FE 0     | 192.168.100.202 | 255.255.255.192 | 192.168.100.193 |



## Firewall ASA 5505

#### Configuracion de red

| Interfaz | Direccion | Mascara         | Nivel de seguridad | Permitro |
| -------- | --------- | --------------- | ------------------ | -------- |
| ETH 0/0  | 10.0.0.6  | 255.255.255.252 | 0                  | Outside  |
| ETH 0/1  | 200.0.0.1 | 255.255.255.0   | 100                | Inside   |

#### Listas de acceso y reenvio de puertos

| ACL           | From | To            | Interface |
| ------------- | ---- | ------------- | --------- |
| httpServerAcl | any  | 200.0.0.10:80 | outside   |
| ICMP-ALLOW    | any  | any icmp      | outside   |

